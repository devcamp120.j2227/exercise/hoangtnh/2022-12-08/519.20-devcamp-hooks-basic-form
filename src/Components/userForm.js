import {useEffect, useState} from "react";

function UserForm (){
    const [firstname, setFirstname] = useState(localStorage.getItem("firstName"));
    const [lastname, setLastname] = useState(localStorage.getItem("lastName"));

    const onChangeFirstNameHandler = (event) =>{
        console.log("firstname is changing...")
        setFirstname(event.target.value);
    }
    const onChangeLastNameHandler = (event) =>{
        console.log("lastname is changing...")
        setLastname(event.target.value);
    }
    useEffect(()=>{
        console.log("ComponentDidUpdate");
        localStorage.setItem("firstName", firstname);
        localStorage.setItem("lastName", lastname);
    })
    return (
        <div>
            <input placeholder="firstname" onChange={onChangeFirstNameHandler} value={firstname}></input>
            <br/>
            <input placeholder="lastname" onChange={onChangeLastNameHandler} value={lastname}></input>
            <p>{lastname} {firstname}</p>
        </div>
    )
}
export default UserForm;